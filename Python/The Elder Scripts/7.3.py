# Задание 7.3
# В прямоугольной матрице размером m×n найдите наименьший элемент и выведите на экран строки,
# в которых этот элемент находится. Размер исходной матрицы задайте самостоятельно.
# Значения элементов матрицы задайте случайным образом. Выведите на экран исходную матрицу,
# значение наименьшего элемента и строки, содержащие этот элемент.

print('Начало программы')
from random import randint

# Максимальное и минимальное значение элемента массива (двузные для удобства вывода на экран)
random_min = 10
random_max = 99

# Обработка пользовательского ввода
while True:
    try:
        m = int(input('Введите количество строк m: '))
        n = int(input('Введите количество столбцов n: '))
        break
    except ValueError:
        print('Повторите ввод')

# Заполнение массива случайными числами в диапазоне
array = [[randint(random_min, random_max) for j in range(n)] for i in range(m)]

# Вывод массива на экран
print('\nВаш массив ', m, '*', n, ':')
for i in range(m):
    for j in range(n):
        print(array[i][j], end=' ')
    print('\n')

input('Дальше\n')

# Массив номеров строк с минимальным значением
min_in_str = []

# Цикл для определения минимального значения
min_value = random_max
for k in range(m):
    if min_value > min(array[k]):
        min_value = min(array[k])

# Цикл для заполнения min_in_str[]
for k in range(m):
    if min_value in array[k]:
        min_in_str.append(k + 1)

print('Минимальное значение элемента массива: ', min_value)

# Ветвление для удобочитаемости
if len(min_in_str) == 1:
    print('Оно встречается в строке', str(min_in_str) + ':\n')
else:
    print('Оно встречается в строках', str(min_in_str) + ':\n')

# Вывод строк, в которых встречается минимальный элемент
for y in range(len(min_in_str)):
    print(min_in_str[y], ': ', array[min_in_str[y] - 1])
print()
print('Конец программы')
input('Нажмите ENTER чтобы выйти')
exit(0)