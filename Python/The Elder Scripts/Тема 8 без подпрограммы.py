import math #math library import
try:
    a, b = float(input('Введите а\n')), float(input('Введите b\n')) #a,b input
except ValueError:
    print('Некорректный ввод')
    print('Конец программы')
    exit(0)
k = 1 #iterator
f1 = 0
f2 = 0
x = a
y = b
try:
    while k <= 5:
        f1 +=(x/(x-y)) ** k
        k +=1
    x = 3
    y = 5
    k = 1
    while k <= 5:
        f2 +=(x/(x-y)) ** k
        k +=1
    y = 1 + math.sin(a) + (f1/f2)
    print("y ≃ %.3f" % y)
except ZeroDivisionError:
        print('Zero division error occured')
finally:
    print('Конец программы')