def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)


def formula(t, tochnost):
    x = abs(t)
    s = 0
    element = x
    k = 3
    prec = 10 ** (-int(tochnost + 2))
    while not element<prec:
        s += element
        element = (x ** k)/factorial(k)
        k += 2
    s = round(s, tochnost)
    if t < 0:
        return -s
    else:
        return s

def tabulate(x1, x2, dx):
    while x1 < x2 + dx/2:
        y = formula(x1, tochnost)
        print(x1, ' - ', y)
        x1 = round(x1, 9) + dx


while True:
    try:
        x1 = float(input('Начальный аргумент (float): '))
        x2 = float(input('Конечный аргумент (float): '))
        if x2<=x1:
            print('Начальное значение аргумента должно быть меньше конечного')
            raise ValueError
        tochnost = int(input('Знаков после запятой (int: 0..9): '))
        if tochnost < 0 or tochnost > 9:
            raise ValueError
        step = abs(float(input('Шаг табуляции: ')))
        if step > (x2-x1):
            print('Шаг табуляции должен быть меньше интервала табуляции')
            raise ValueError
        break
    except ValueError:
        print('Ошибка ввода, повторите')

f = tabulate(x1, x2, step)
print(f)