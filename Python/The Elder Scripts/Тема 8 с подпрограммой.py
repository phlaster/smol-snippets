import math #math library import
def f(x,y):
    k = 1
    result = 0.0
    while k <= 5:
        result +=(x/(x-y)) ** k
        k +=1
    return result
def y(x,y):
    result = float(1 + math.sin(x) + (f(x,y))/(f(3,5)))
    return result    
true = True
while true == True:
    try:
        a, b = float(input('Введите а\n')), float(input('Введите b\n')) #a,b input
        true = False
    except ValueError:
        print('Некорректный ввод')
        print('Повторите')
try:
    itog = y(a,b)
    print("y ≃ %.3f" % itog)
except ZeroDivisionError:
    print('Zero division error occured')
except:
    print('Unknown error occured')
finally:
    print('Конец программы')