#!/usr/bin/env python
# coding: utf-8
# -*- coding: utf-8 -*-
import os
import sys
import numpy as np
import hashlib as hl


# Кастомный ексепшн для файлов >100Mb
class FileTooBigError(Exception):
    """ Error used when trying to open file bigger than 100Mb 
        message -- explanation of the error
    """

    def __init__(self, message="Нельзя обрабатывать файлы более 100Мб"):
        self.message = message
        super().__init__(self.message)



# Ввод текста для зашифровки с клавиатуры

def text_read():
    text_in = input("Введите текст для шифрования:\n")
    temp = 'output' + '.txt' #Создаёт временный файл с введённым текстом
    
    with open(temp, 'wb') as file_proc:
        file_proc.write(text_in.encode())
    
    return temp #Вернёт имя временного файла

def get_passphrase():
    print("Введите ваш пароль:\n")
    passphrase = input()
    return passphrase

#Преобразование ключегового слова в сид рандомайзера

def ranseed(keyphrase):
#    hsh = hl.sha1(keyphrase.encode())
#    text = keyphrase.encode(encoding="ascii",errors="xmlcharrefreplace")
#    hsh.update(text)
    seed = int(hl.sha1(keyphrase.encode()).hexdigest(), 16) // 10**39
    return seed


# Берёт сид и temp файл и создаёт рядом зашифрованный enc_temp 

def encrypt(filename,seed):
    if os.path.getsize(filename) > 100000000:
        raise FileTooBigError
        
    np.random.seed(ranseed(seed))
    
    with open(filename, 'rb') as source:
        text = np.array(list(source.read()), dtype=np.uint8)
        
    size = len(text)
    encryptor = np.random.randint(256, size=size, dtype=np.uint8)
    result = text+encryptor
    
    with open(filename+'.enc','wb') as res:
        res.write(result.tobytes())
        
    return size


# Берёт зашифрованный файл и сид и создаёт dec_файл
def decrypt(filename, seed):
    if os.path.getsize(filename) > 100000000:
        raise FileTooBigError
        
    np.random.seed(ranseed(seed))
    with open(filename, 'rb') as source:
        text = np.array(list(source.read()), dtype=np.uint8)
        
    decryptor = np.random.randint(256, size=len(text), dtype=np.uint8)
    result = text-decryptor
    size = len(text.tobytes())
    
    with open(filename+'.dec','wb') as res:
        res.write(result.tobytes())
        
    return size


# Начало программы
def main():
    print("Наивная реализация двустороннего криптографического алгоритма на Python3.\n")
    print("Шифруем или расшифровываем? Введи 'in' или 'out': ")
    while True:
        in_or_out = input()
        if in_or_out == 'in':
            txt_or_file()
            return
        elif in_or_out == 'out':
        	print("Введи имя файла: ")
        	while True:
        		try:
		            filename = input()
		            passphrase = get_passphrase()
		            print("Расшифровываем...\n")
		            size = decrypt(filename,passphrase)
		            print(f"Расшифровано {size} байт.")
		            break
		        except Exception as e:
		            print(e)
		            continue
		        break
        break


def txt_or_file():
    print("Текст, введённый вручную или читаем файл? 't' или 'f': ")
    while True:
        txt_or_file = input()
        if txt_or_file == 't':
            tempfile = text_read()
            passphrase = get_passphrase()
            print("Шифруем...\n")
            size = encrypt(tempfile,passphrase)
            print(f"Зашифровано {size} байт, не забудьте пароль.")
            os.remove(tempfile)
            
        elif txt_or_file == 'f':
            print("Введите имя файла:\n")
            while True:
                try:
                    filename = input()
                    passphrase = get_passphrase()
                    print("Шифруем...\n")
                    size = encrypt(filename,passphrase)
                    print(f"Зашифровано {size} байт, не забудьте пароль.")
                    break
                except FileNotFoundError as e:
                    print(f"{e}\nВведите другое имя")
                    continue

        else:
            continue
        return

if __name__ == "__main__":
    excode = 1
    try:
        print("\n\nТак, сразу предупреждаю!\n\
Скрипт не оптимизирован!\n\
В целях экономии оперативной памяти не обрабатывает файлы больше 100Мб\n\
Помещает итоговый файл в папку рядом с исходным\n\
Только в ознакомительных целях!\n")
        main()
        input("Нажмите любую клавишу")
        excode = 0
    except Exception as e:
        print('\nError: %s' % e, file=sys.stderr)
    sys.exit(excode)
