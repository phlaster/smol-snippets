#!/usr/bin/env python
# coding: utf-8
# -*- coding: utf-8 -*-
from datetime import datetime
import numpy as np
from scipy.ndimage import gaussian_filter as gf
from scipy.signal import find_peaks
import scipy
import re
import math as m
import os

import matplotlib.pyplot as plt
from bokeh.plotting import figure, show, output_file
from bokeh.io import output_notebook
from bokeh.layouts import gridplot

# Константы
TOOLS = "hover,pan,wheel_zoom,box_zoom,reset,save" #инструменты Bokeh
DEFAULT = "samples"+os.sep+"000.txt" # Тестовый файл из папки samples
PATH = os.getcwd()+os.sep+"plots-out"
DEFAULTFRAME = (82, 95) # удобный отрезок для наблюдений
PEAKDIST = 40 #минимальное расстояние между пиками, подбиралось вручную

def main():

    print("\n\nСкрипт для визуализации затухающих колебаний\
и рассчёта логарифмического декремента.\n\
Не рекоммендуется для других целей.\n")
    
    # Создание директории для результатов
    if not os.path.isdir(PATH):
        os.mkdir(PATH)
    print(f"Результат будет записан в {PATH}")

    isdefault = False # Для файлов, кроме стандартного демонстрационного
    
    inputname = input(f"Введите имя файла или ENTER для открытия {DEFAULT}: ")
    if inputname == '':
        filename = DEFAULT
        isdefault = True
        print("\nВ примере по умолчанию находятся экспериментальные результуты\
, полученные непосредственно на занятии.\n\
В файл подряд записано несколько колебательных циклов с разными начальными\
углами. Не рекоммендуется задавать слишком широкий интервал.")
    else: filename = inputname

    with open(filename, encoding="utf-8", errors='ignore') as file:
        table = file.read()
            
    #ищет позицию в строке, где начинаются числа
    findstart = re.search("\d",table).span()[0]

    #отсекает начало, разбиратеся с запятыми, делит на список, приводит к float
    if filename.endswith('.csv'):
        table = np.array(
        table[findstart:].replace(',',' ').split()
        ).astype(np.float) # для импорта из Algodoo
    else:
        table = np.array(
        table[findstart:].replace(',','.').split()
        ).astype(np.float) # для прочего

    # Сырые координаты в виде 2-мерного массива([ [OX][OY] ])
    rawcord = np.array([table[::2],table[1::2]])


    dt = round(rawcord[0,2]-rawcord[0,1], 5) # Определяет шаг OX
    freqmin = dt*PEAKDIST
    freqmax = 1/freqmin
    print(f"Невозможно работать с неравномерно распределёнными значениями!\n\
Определённый шаг развёртки: {dt} с\n\
Невозможно корректно обработать колебания с частотой > {freqmax} Гц\n\
Не рекоммендуется обрабатывать колебания с частотой < {freqmin} Гц")

    
    # Предварительный просмотр через MPL
    plt.xlabel('time, s.')
    plt.ylabel('Angle, deg.')
    plt.plot(rawcord[0],rawcord[1])
    input("\nСейчас откроется окно предварительного просмотра. Выберите\
интересующий временной интервал и заметьте ординату нулевой амплитуды.\n\
После закрытия окна будет предложено их указать для дальнейшего анализа.\n\
Автоматическое определение нуля не всегда даёт лучший результат.\n\
ENTER - дальше")
    plt.show()


    # Обрезка
    inputframe = tuple(map(float, input("\n\
    Введите временные рамки (два числа через пробел) для анализа или будет\
задано умолчание: ").replace(',','.').split() ))[:2]
    if isdefault:
        frame = DEFAULTFRAME if inputframe==() else inputframe
    else:
        frame = (rawcord[0,0],rawcord[0,-1]) if inputframe==() else inputframe

    if min(frame) < rawcord[0,0]:
        frame = (rawcord[0,0], max(frame))
    if max(frame) > rawcord[0,-1]:
        frame = (min(frame), rawcord[0,-1])
        
    print("Выбран интервал: ", min(frame), "..", max(frame))
    
    # Находит индексы пересечения двух массивов от [0] до max и от min до [:-1]
    frame = np.intersect1d(np.where(rawcord[0]>=min(frame)), np.where(rawcord[0]<=max(frame)))

    # Сырые координаты, обрезанные по веремени
    cutcords = np.array([rawcord[0,frame], rawcord[1,frame]])

    # 0 вычисляется как среднее всех значений на промежутке
    autozero = np.average(cutcords[1])
    inputzero = input(f"\nАвтоматически определена ордината нулевой амплитуды\
на {'%.4f' %autozero}\n\
ENTER или введите значение вручную. ").replace(',','.')
    
    zero = autozero if inputzero == '' else float(inputzero)
    print(f"Выбран ноль на {'%.4f' %zero}")

    #линия нулевой амплитуды
    zeroline = np.array([ [cutcords[0,0],cutcords[0,-1]], [zero,zero] ])

    # Координаты после сглаживания по Гауссу
    gaussed = np.array([cutcords[0],gf(cutcords[1], sigma = 5, truncate=0.5)])

    # Ищем экстремумы
    peaks , _= find_peaks(cutcords[1], height=zero, distance=PEAKDIST)
    pcords = np.array([cutcords[0,peaks], cutcords[1,peaks]])
    
    # Количество пиков
    npikes = len(pcords[0])
    
    # Количество исследуемых промежутков (целых волн)
    nwaves = npikes-1
    
    # Массив значений частоты
    freq = np.array([1/(pcords[0,i+1]-pcords[0,i]) for i in range(nwaves)])
    
    # Массив значений декремента
    logdec = np.array(
        [m.log(abs((pcords[1,i]-zero)/(pcords[1,i+1]-zero)))
        for i in range(nwaves)])


    # Первый график
    p1 = figure(
        title="Полная картина",
        x_axis_label='Время, с',
        y_axis_label='Угол, град.')
    p1.line(rawcord[0],rawcord[1],
                                color='black',
                                line_width=1)
    
    # Второй
    p_peaks = figure(
        title="Найденные пики на сглаженном участке",
        x_axis_label='Время, с',
        x_range=[cutcords[0,0]-0.5,cutcords[0,-1]+0.5],
        y_axis_label='Угол, град.',
        tools=TOOLS)
        
    p_peaks.line(cutcords[0], cutcords[1]-zero,
        legend_label='Исходные',
        color='black',
        line_width=1)
        
    p_peaks.line(gaussed[0], gaussed[1]-zero,
        legend_label='Сглаженные',
        color='darkgrey',
        line_width=2)
        
    p_peaks.circle(pcords[0], pcords[1]-zero,
        radius=.08,
        legend_label='Пики (по сглаженным)',
        color="firebrick",
        alpha=0.9)
        
#    p_peaks.line(zeroline[0], zeroline[1]-zero,
#        legend_label='Линия нулевой амплитуды',
#        color='red',
#        line_width=1.5)
    
    # Третий
    p_decrement = figure(
        title="Логарифмический декремент",
        x_axis_label='Время, с',
        x_range=[cutcords[0,0]-0.5,cutcords[0,-1]+0.5],
        y_range=[0, max((max(freq),max(logdec)))*1.1],
        tools=TOOLS)
        
    p_decrement.line(pcords[0, :-1],freq,
        line_width=4,
        legend_label='Частота, 1/с',
        color="darkgrey",
        alpha=0.4)
        
    p_decrement.line(pcords[0, :-1],logdec,
        line_width=4,
        legend_label='Декремент',
        color="blue",
        alpha=0.6)

    # Построение в колонну
    p = gridplot([p1, p_peaks, p_decrement],
        ncols=1,
        plot_width=950,
        plot_height=400)
    
    # Запись на диск и вывод
    curdata = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
    output_file(PATH+os.sep+"Plot"+"_"+curdata+".html")
    show(p)
    
if __name__ == "__main__":
    ec = 1
    try:
        main()
        ec = 0
        exit(ec)
    except Exception as e:
        print(e)
        exit(ec)


