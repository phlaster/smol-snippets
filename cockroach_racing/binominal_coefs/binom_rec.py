#!/usr/bin/env python
# Рассчёт финоминального коэффициента по рекуррентной формуле
# Это. Просто. Очень. Медленно.

def C_rec(k: int, n: int) -> int:
    if k > n:
        return 0
    elif k <= 0:
        return 1
    else:
        return C_rec(k-1, n-1) + C_rec(k, n-1)


if __name__ == "__main__":    
    k,n = map(int, input("Введите k,n ").split())
    print(C_rec(k,n))
    
