#!/usr/bin/env python
# Рассчёт биноминальных коэфициентов школьной формулой с факториалами
# При слишком больших входных числах, в 8 строке рекурсивный вызов
# факториала переполняет стек. Можно заменить на math.factorial, с
# которым не будет возникать таких проблем

def C_fac(k: int, n: int) -> int:
    fac = lambda x: 1 if x < 2 else x * fac(x-1)
    return fac(n) // (fac(n-k) * fac(k))


if __name__ == "__main__":    
    k,n = map(int, input("Введите k,n ").split())
    print(C_fac(k,n))
    
